;; user-init.el -- User initialistion file
;; Load mode specific init files

(require 'init-ace-jump)
(require 'init-helm)
(require 'init-python)
(require 'init-func)
(require 'init-magit)
(require 'init-ido)
(provide 'user-init)
