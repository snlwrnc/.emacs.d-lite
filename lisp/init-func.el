;; MY FUNCTIONS
;; --------------------------------------

;; duplicate current line
(defun duplicate-line()
  (interactive)
  (move-beginning-of-line 1)
  (kill-line)
  (yank)
  (open-line 1)
  (next-line 1)
  (yank)
)
(global-set-key (kbd "C-c C-;") 'duplicate-line)

(provide 'init-func)
