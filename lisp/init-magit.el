;; MAGIT
;; --------------------------------------
;; https://magit.vc/

;; info about current git repo
(global-set-key (kbd "C-x g") 'magit-status)

;; supress buffer reverting on magit window close
(setq magit-bury-buffer-function 'quit-window)

(provide 'init-magit)
