;; PYTHON CONFIGURATION
;; --------------------------------------

(setq elpy-rpc-backend "jedi")
(pyvenv-activate "/home/slawrence/anaconda3/envs/main")

(elpy-enable)
;; set ipython as default interpreter 
(setq python-shell-interpreter "ipython"
      python-shell-interpreter-args "-i --simple-prompt")

;; use flyckeck not flymake with elpy
(when (require 'flycheck nil t)
  (setq elpy-modules (delq 'elpy-module-flymake elpy-modules))
  (add-hook 'elpy-mode-hook 'flycheck-mode))

;; enable autopep8 formatting on save
(require 'py-autopep8)
(add-hook 'elpy-mode-hook 'py-autopep8-enable-on-save)

;; enable code folding, change key
(add-hook 'elpy-mode-hook 'hs-minor-mode)
;; (define-key hs-minor-mode-map (kbd "C-c h") (lookup-key hs-minor-mode-map (kbd "C-c @")))
;; (define-key hs-minor-mode-map (kbd "C-c @") nil)

;; mouse shortcuts
(define-key elpy-mode-map (kbd "<mouse-8>") 'elpy-shell-send-statement)
(define-key elpy-mode-map (kbd "<mouse-9>") 'elpy-shell-send-region-or-buffer)

;; virtual environment directory
;;(setenv "WORKON_HOME" "/home/slawrence/anaconda3/envs/")

(provide 'init-python)
